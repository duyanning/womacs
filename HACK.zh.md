# 调试办法

首先，执行uninstall.bat以删除文件

`"%userprofile%\Application Data\Microsoft\Word\Startup\womacs.dotm"`

这个文件的自动加载会干扰我们开发。



区分两个模板：

- `womacs-dev.dotm`	这是写代码的地方（但我们并不直接打开该`dotm`模板，而是打开使用该模板的`docx`文档。这种`docx`怎么产生呢？只要双击`womacs-dev.dotm`就能产生）

- `womacs.dotm`		这是发布的模板（运行`womacs-dev.dotm`里`DeveloperTools`模块里的`generate_template_for_publishing`过程来产生发布用`womacs.dotm`）


